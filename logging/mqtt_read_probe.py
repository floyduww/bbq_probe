#!/usr/bin/python

import paho.mqtt.client as mqtt
import logger

def on_connect(self,mosq, obj, rc):
    mqttc.subscribe(("nano/sensor/meattemp-1/#", 0))
    print("rc: " + str(rc))


def on_message(mosq, obj, msg):
    print(msg.topic + " " + str(msg.payload))
    topicParts = msg.topic.split("/")
    if(len(topicParts) == 5) :
        logger.writeLog("bbq",topicParts[3] + ": " + str(msg.payload.decode("utf-8").strip()))
        logger.writeStatus(topicParts[3], str(msg.payload.decode("utf-8").strip()))

def on_subscribe(mosq, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


mqttc = mqtt.Client()
# Assign event callbacks
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_subscribe = on_subscribe
# Connect
mqttc.connect("MQTTSERVER", 1883, 60)


# Continue the network loop
mqttc.loop_forever()
