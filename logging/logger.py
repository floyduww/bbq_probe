import datetime
import time

def writeLog(device, data):
    currDate = datetime.date.today()
    ts = time.time()
    
    filename = "logs/" + str(currDate) + "-" + device + ".log.txt"
    
    entry = str(ts) + " " + str(data) + "\n"
    
    with open(filename, 'a+') as f: f.write(entry)
     
    
def writeStatus(device, data):
    currDate = datetime.date.today()
    ts = time.time()
    
    filename = "logs/" + device + ".txt"
    
    entry = str(ts) + " " + str(data) + "\n"
    
    with open(filename, 'w') as f: f.write(entry)
