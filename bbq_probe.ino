/*
    Arduino nano thermister and HC-SR04 utlrasonic sensor

*/

#include <WiFiEspClient.h>
#include <WiFiEsp.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>

//WiFi settings
SoftwareSerial soft(8, 9); // RX, TX
#define WIFI_SSID "YOURWIFISSID"       // Your WiFi ssid
#define PASSWORD "YOURWIFIPASSWORD"         // Password
int status = WL_IDLE_STATUS;
WiFiEspClient espClient;

//MQTT settings
const char* MQTT_SERVER = "MQTT_SERVER_IP";
#define CLIENT_ID       "meattemp-1"
#define INTERVAL        20000 // 20 sec between checks
PubSubClient client(espClient);

// temp probe settings
// which analog pin to connect
#define PIN1 A1
#define PIN2 A2
#define PIN3 A3
const char NAME1[10] = "Meat1";
const char NAME2[10] = "Meat2";
const char NAME3[10] = "Meat3";

// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 20
// the value of the 'other' resistor
#define SERIESRESISTOR 9880
//float A1c[] = {0.3075696628e-03,  3.387076026e-04,  -3.435715123e-07};
float A1c[] = {2.227256348e-03, 0.2605236604e-04, 4.204952537e-07};
float A2c[] = {0.2187197775e-03, 2.845982428e-04, -0.8815570498e-07};
float A3c[] = {0.2187197775e-03, 2.845982428e-04, -0.8815570498e-07};

//HC-SR04 settings
// pins for distance sensor
const int TRIGPIN = 3;
const int ECHOPIN = 2;

const float MAXLEV = 26.0; // max pellet level



void setup() {
    Serial.begin(115200);
    Serial.println("running");

    analogReference(EXTERNAL);

    initWiFi();

    mqttConnect();

    pinMode(TRIGPIN, OUTPUT);
    pinMode(ECHOPIN, INPUT);
}



void loop(void) {
        
    if ( status != WL_CONNECTED) {
        initWiFi();
    } else if (client.connected() != true) {
        mqttConnect();
    } else {
        readProbe(PIN1, NAME1, A1c);
        readProbe(PIN2, NAME2, A2c);
        readProbe(PIN3, NAME3, A3c);
        distance();
        delay(INTERVAL);
    }
}



void initWiFi() {
    // initialize serial for ESP module
    soft.begin(9600);
    // initialize ESP module
    WiFi.init(&soft);
    // check for the presence of the shield
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue
        while (true);
    }

    Serial.println("Connecting to AP ...");
    // attempt to connect to WiFi network
    while(status != WL_CONNECTED) {
        Serial.print("Attempting to connect to WPA SSID: ");
        Serial.println(WIFI_SSID);
        // Connect to WPA/WPA2 network
        status = WiFi.begin(WIFI_SSID, PASSWORD);
        delay(500);
    }
    
    Serial.println("Connected to AP");
}



void mqttConnect() {
    char topicTemp[40];

    Serial.print("Attempting MQTT connection...");
          
    // Attempt to connect
    String topicString = "nano/sensor/" + String(CLIENT_ID) + "/status";
    topicString.toCharArray(topicTemp, topicString.length() + 1);
         
    client.setServer(MQTT_SERVER, 1883);
    client.connect(CLIENT_ID, topicTemp, 0, 1, "Offline");
    client.publish(topicTemp, "Online", 1);
    Serial.print(client.state());
}



void readProbe(int probePIN, char * probeName, float c[]) {
    float average, fst, st;
    char bufferProbe[8];
    
    average = getReading(probePIN);
    st = steinharthart(average, c);
    fst = (st * 9.0) / 5.0 + 32.0;

    Serial.print("Temperature st ");
    Serial.print(st);
    Serial.print(" *C  ");
    Serial.print(fst);
    Serial.println(" *F");

    dtostrf(fst, 7, 1, bufferProbe);
    sendProbeData (bufferProbe, probeName);
}



float getReading(int probePIN) {
    float average = 0;

    // take N samples in a row, with a slight delay
    for (int i = 0; i < NUMSAMPLES; i++) {
        average += analogRead(probePIN);
        delay(10);
    }

    average /= NUMSAMPLES;
    average = round(average);

    Serial.print("Average analog reading ");
    Serial.println(average);

    return average;
}



float steinharthart(float average, float c[]) {
    float c1, c2, c3, R2, logR2, T, Tc;

    c1 = c[0];
    c2 = c[1];
    c3 = c[2];
    R2 = SERIESRESISTOR / (1023.0 / (float)average - 1.0);
    logR2 = log(R2);
    T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
    Tc = T - 273.15;

    return Tc;
}



void distance() {
    float duration, distance, level;
    char bufferDist[7];
    
    digitalWrite(TRIGPIN, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIGPIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIGPIN, LOW);

    duration = pulseIn(ECHOPIN, HIGH);
    distance = (duration * .0343) / 2;

    level = MAXLEV - distance;
    if(level < 0) {
        level = 0;
    }

    if (level > MAXLEV) {
        level = MAXLEV;
    }

    Serial.print("Distance: ");
    Serial.print(distance);
    Serial.print(" : ");
    Serial.println(level);

    dtostrf(level, 6, 1, bufferDist);
    sendProbeData (bufferDist, "fuel");
}



void sendProbeData(char * msg, char * probe) {
    char topic[40];
    
    String topicString = "nano/sensor/" + String(CLIENT_ID) + "/" + String(probe) + "/status";
    topicString.toCharArray(topic, topicString.length() + 1);
    client.publish(topic, msg, 1);
}
