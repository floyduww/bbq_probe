# bbq_probe

I just started up this project cause I got sick of walking outside to check the 
temps on my smoker.  I had most of the parts and some of the code lying around.
Also while I was at it I decided to add on a pellet sensor for $5 instead
of the $80 that Traeger wants.   So far I have only smoked with this setup once,
and it went well with only a couple bugs. This whole project would run you about $25 
in parts, and since most can not be bought as singles you will have parts for your 
next project.

**Known Issues**
*  Wifi can drop and it didn't reconnect properly (power cycle would momentarily fix), need to move the antenna
*  The first probe I used could not give me chamber temps when grill on high
*  probes are not 100% calibrated

**What it can do**
*  monitor 3 meat temps, (or smoker temp up to 250 on my first run)
*  measure pellet amount
*  send data to mqtt server

**What it can't do**
*  control the smoker in any way
*  local display at the smoker (but a disaply can be added)
*  this part does not handle any display or data processing, to do that you need to get the information from the mqtt server in realtime

**Wiring diagram coming soon**

main parts

*  arduino nano V3.0 atmega328p
*  esp8266-01 wifi
*  temp probe (j21) from aliexpress
*  HC-SR04 Utlrasonic distancse sensor
*  TIP120-R transiter (3.3V)
*  2.5mm mono headphone jacks
*  mirco USB powersupply, 2.5, 5V (Really only need 1A if you have it lying around)
*  10 ohm resistors (may have to change to 100 ohm)
*  female micro usb to dip 5-pin pinboard
 
misc parts

*  breakaway PCB board headers
*  protoboard
*  jumper wire
*  balsa wood
*  hot glue
*  basic soldering tools and skill
*  micro usb cable to program arduino

other required stuff

*  mqtt server
*  mqtt client
*  home wireless

other notes
*  esp8266 must be flashed to serial mode
*  esp9266 must be set up 9600 baud

Links
*  [Basic thermistor tutorial](https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/)
*  [Arduino Nano Wifi setup](https://www.instructables.com/id/ESP8266-Wifi-Temperature-Logger/)
*  [Thermistor Calculator](https://www.thinksrs.com/downloads/programs/therm%20calc/ntccalibrator/ntccalculator.html)
